package com.themakers.mavesaservice.Activities;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.themakers.mavesaservice.Activities.ClearMechanic.ClearMechanicActivity;
import com.themakers.mavesaservice.Activities.ClearMechanic.WebViewActivity;
import com.themakers.mavesaservice.Controllers.UserController;
import com.themakers.mavesaservice.Models.User;
import com.themakers.mavesaservice.R;

public class MenuActivity extends AppCompatActivity  implements  View.OnClickListener{

    RelativeLayout btnLocation,btnClearMechanic,btnVehicle, btnDetailVehicle, btnCuenta;
    ;

    //SQLITE
    UserController userControllerInstance;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        setToolbar();

        // toolbar.setTitle("Menu");
        btnLocation= (RelativeLayout) findViewById(R.id.btnLocation);
        btnClearMechanic= (RelativeLayout)findViewById(R.id.btnClearMechanic);
        btnVehicle= (RelativeLayout)findViewById(R.id.btnVehicle);
        btnDetailVehicle = (RelativeLayout)findViewById(R.id.btnDetailVehicle);
        btnCuenta = (RelativeLayout)findViewById(R.id.btnCuenta);

        btnCuenta.setOnClickListener(this);
        btnLocation.setOnClickListener(this);
        btnClearMechanic.setOnClickListener(this);
        btnDetailVehicle.setOnClickListener(this);




        AnimationsLoad();

        Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();

        loadInstances();
    }

    ///Instances Class
    private void loadInstances() {
        this.userControllerInstance= UserController.getInstance(this);
    }

    private void AnimationsLoad() {
        if (Build.VERSION.SDK_INT >= 21){
            setupWindowAnimations();
            Window window = getWindow();
            Transition t3 = TransitionInflater.from(this)
                    .inflateTransition(R.transition.detail_enter_transition);
            window.setEnterTransition(t3);
        }
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        getWindow().setReenterTransition(new Explode());
        getWindow().setExitTransition(new Explode().setDuration(500));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLocation:
                Intent i = new Intent(MenuActivity.this, WorkshopsActivity.class);
                startActivity(i);

              /* int position= 1;
                VehicleActivity.launch(
                         MenuActivity.this, position);*/
                break;

            case R.id.btnClearMechanic:
                Intent t = new Intent(MenuActivity.this, WebViewActivity.class);
                startActivity(t);
                break;

            case R.id.btnVehicle:
                Intent s = new Intent(MenuActivity.this, ClearMechanicActivity.class);
                startActivity(s);
                break;
            case R.id.btnDetailVehicle:
                Intent d = new Intent(MenuActivity.this, VehicleDetailActivity.class);
                startActivity(d);
                break;

            case R.id.btnCuenta:
                Intent c = new Intent(MenuActivity.this, Info_User_Activity.class);
                startActivity(c);
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void launch(LoginActivity context, int position) {
        Intent intent = new Intent(context, MenuActivity.class);
        // intent.putExtra(EXTRA_POSITION, position);
        // Los elementos 4, 5 y 6 usan elementos compartidos,
        ActivityOptions options0 = ActivityOptions.makeSceneTransitionAnimation(context);
        context.startActivity(intent,options0.toBundle());
        closedActivity(context);
        //context.finishAfterTransition();
    }

    //Cerrar Activity Anterior
    private static void closedActivity(final LoginActivity context) {
        int myTimer= 2000;
        /// Toast.makeText(this, "Cargando...", Toast.LENGTH_LONG).show();
        new android.os.Handler().postDelayed(new Runnable(){
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run(){
                context.finish();
            }
        }, myTimer);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmacion");
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i("Dialogos", "Confirmacion Cancelada.");
            }
        });
        builder.setMessage("¿Cerrar Sesión?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                userControllerInstance.abrirBaseDeDatos();
                User user= userControllerInstance.getFirstUser();
                userControllerInstance.updateLoginSesion(user.getUser_Id(),false);
                startActivity(new Intent(getBaseContext(), LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                userControllerInstance.cerrar();
                finish();
            }
        });
        builder.setIcon(R.drawable.ic_logo_mavesa_service);
        builder.show();
    }
}
