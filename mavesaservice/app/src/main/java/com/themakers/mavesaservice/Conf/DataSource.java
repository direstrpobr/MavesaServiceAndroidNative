package com.themakers.mavesaservice.Conf;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by EnuarMunoz on 31/07/2017.
 */

public class DataSource extends SQLiteOpenHelper {
    // información del a base de datos
    static final String DB_NAME = "mavesaservice.db";
    static final int DB_VERSION = 1;

    ////Tabla Usuarios (Users)
    /*--------------------------------------------------------------------------------------------*/
    public static final String TABLE_USERS = "Users";
    public static final String User_Id = "id";
    ///Atributos Usuarios
    public static final String Nombre_Cliente = "Name_User";
    public static final String Codigo_Cliente = "Codigo_Cliente";
    public static final String Numero_Cedula = "Numero_Cedula";
    public static final String Identificacion = "Identificacion";
    public static final String Direccion = "Direccion";
    public static final String Ciudad = "Ciudad";
    public static final String Telf_Fijo = "Telf_Fijo";
    public static final String Telf_Movil = "Telf_Movil";
    public static final String Direccion_Electronica = "Direccion_Electronica";
    public static final String Fecha_Nacimiento = "Fecha_Nacimiento";
    public static final String Sexo = "Sexo";
    public static final String Is_Admin = "Is_Admin";
    public static final String Workshop_Id = "Workshop_Id";
    public static final String IsRemembered = "IsRemembered";

    private static final String CREATE_TABLE_USERS = "create table "
            + TABLE_USERS + "(" + User_Id
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + Nombre_Cliente + " TEXT NULL,"
            + Codigo_Cliente + " TEXT NULL,"
            + Numero_Cedula + " TEXT NULL,"
            + Identificacion + " INTEGER NULL,"
            + Direccion + " TEXT NULL,"
            + Ciudad + " TEXT NULL,"
            + Telf_Fijo + " TEXT NULL,"
            + Telf_Movil + " TEXT NULL,"
            + Direccion_Electronica + " TEXT NULL,"
            + Fecha_Nacimiento + " TEXT NULL,"
            + Sexo + " TEXT NULL,"
            + Is_Admin + " BOOLEAN NULL,"
            + Workshop_Id + " INTEGER NULL,"
            + IsRemembered + " BOOLEAN NULL);";
    public DataSource(Context context) {
        super(context, DB_NAME, null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stu
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USERS);
        onCreate(db);
    }
}
