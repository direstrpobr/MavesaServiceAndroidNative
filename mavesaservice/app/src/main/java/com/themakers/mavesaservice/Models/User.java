package com.themakers.mavesaservice.Models;

/**
 * Created by EnuarMunoz on 05/08/2017.
 */

public class User {

    //Atributes
    public int User_Id;
    public String Nombre_Cliente;
    public String Codigo_Cliente;
    public String Numero_Cedula;
    public int Identificacion ;
    public String Direccion ;
    public String Ciudad ;
    public String Telf_Fijo ;
    public String Telf_Movil ;
    public String Direccion_Electronica ;
    public String Fecha_Nacimiento ;
    public String Sexo ;
    public boolean Is_Admin ;
    public int Workshop_Id;
    public boolean IsRemembered;



    //Constructor
    public User() {
        this.User_Id = User_Id;
        Nombre_Cliente = Nombre_Cliente;
        Codigo_Cliente = Codigo_Cliente;
        Numero_Cedula = Numero_Cedula;
        Identificacion = Identificacion;
        Direccion = Direccion;
        Ciudad=Ciudad;
        Telf_Fijo= Telf_Fijo;
        Telf_Movil= Telf_Movil;
        Direccion_Electronica=Direccion_Electronica;
        Fecha_Nacimiento= Fecha_Nacimiento;
        Sexo= Sexo;
        Is_Admin= Is_Admin;
        Workshop_Id= Workshop_Id;
        IsRemembered=IsRemembered;
    }




    //Methods
    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public String getNombre_Cliente() {
        return Nombre_Cliente;
    }

    public void setNombre_Cliente(String nombre_Cliente) {
        Nombre_Cliente = nombre_Cliente;
    }

    public String getCodigo_Cliente() {
        return Codigo_Cliente;
    }

    public void setCodigo_Cliente(String codigo_Cliente) {
        Codigo_Cliente = codigo_Cliente;
    }

    public String getNumero_Cedula() {
        return Numero_Cedula;
    }

    public void setNumero_Cedula(String numero_Cedula) {
        Numero_Cedula = numero_Cedula;
    }

    public long getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(int identificacion) {
        Identificacion = identificacion;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String ciudad) {
        Ciudad = ciudad;
    }

    public String getTelf_Fijo() {
        return Telf_Fijo;
    }

    public void setTelf_Fijo(String telf_Fijo) {
        Telf_Fijo = telf_Fijo;
    }

    public String getTelf_Movil() {
        return Telf_Movil;
    }

    public void setTelf_Movil(String telf_Movil) {
        Telf_Movil = telf_Movil;
    }

    public String getDireccion_Electronica() {
        return Direccion_Electronica;
    }

    public void setDireccion_Electronica(String direccion_Electronica) {
        Direccion_Electronica = direccion_Electronica;
    }

    public String getFecha_Nacimiento() {
        return Fecha_Nacimiento;
    }

    public void setFecha_Nacimiento(String fecha_Nacimiento) {
        Fecha_Nacimiento = fecha_Nacimiento;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public boolean getIs_Admin() {
        return Is_Admin;
    }

    public void setIs_Admin(boolean is_Admin) {
        Is_Admin = is_Admin;
    }

    public int getWorkshop_Id() {
        return Workshop_Id;
    }

    public void setWorkshop_Id(int workshop_Id) {
        Workshop_Id = workshop_Id;
    }

    public boolean getIsRemembered() {
        return IsRemembered;
    }

    public void setIsRemembered(boolean remembered) {
        IsRemembered = remembered;
    }


}
