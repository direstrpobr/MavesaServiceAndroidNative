package com.themakers.mavesaservice.Activities;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Region;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.transition.Fade;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.transition.Explode;
import android.transition.Slide;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionButton;
import com.themakers.mavesaservice.Activities.Workshop_Assistant.Menu_Workshop_Assistant_Activity;
import com.themakers.mavesaservice.Controllers.UserController;
import com.themakers.mavesaservice.Models.User;
import com.themakers.mavesaservice.R;
import com.themakers.mavesaservice.Services.ApiServices.Routes.Const;
import com.themakers.mavesaservice.Services.Aplication.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.etNumeroCedula)
    AutoCompleteTextView etNumeroCedula;
    @Bind(R.id.sign_in_button)
    com.github.clans.fab.FloatingActionButton sign_in_button;

    @Bind(R.id.progressBarIndeterminate)
    ProgressBar progressBarIndeterminate;
    //Animaciones
    // UI references.
    boolean Is_Remembered=true;
    boolean Is_Asistant_Workshop=false;
    //Conections
    private ProgressDialog pDialog;

    //ROUTES
    private final static String VERIFICATE_USER_EXIST = "clienteexiste/";
    private final static String VERIFICATE_USER_DETAILS = "clientedatos/";

    //SQLITE
    UserController userControllerInstance;

    //Float Button
    //FloatingActionButton sign_in_button;
    // com.github.clans.fab.FloatingActionButton sign_in_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);



        //Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        //Quitar Barra de Navegacion
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        animationsLoad();
        showSnakBar(R.color.colorPrimary," Bienvenido a MavesaService");


        loadInstances();
        verificateLoginIsRemembered();
    }

    ///Instances Class
    private void loadInstances() {
        this.userControllerInstance= UserController.getInstance(this);
    }


    /*Events Clicks*/
    /*-------------------------------------------------------------------------------------------------*/

    @OnClick(R.id.sign_in_button)
    public void handleClickSubmit() {
        hideKeyboard();
        attemptLogin();
    }

    @OnCheckedChanged(R.id.switchRemenbered)
    public  void onGenderSelectedRemenbered(CompoundButton button, boolean isChecked){
        //do your stuff.
        if (isChecked) {
            Is_Remembered=true;
            showSnakBar(R.color.colorPrimary," Recordatorio: "+ String.valueOf(Is_Remembered));
        } else {

            Is_Remembered=false;
            showSnakBar(R.color.colorPrimary," Recordatorio: "+ String.valueOf(Is_Remembered));
        }
    }

    @OnCheckedChanged(R.id.switchAsistantWorkshop)
    public  void onGenderSelectedAsistantWorkshop(CompoundButton button, boolean isChecked){
        //do your stuff.
        if (isChecked) {
            Is_Asistant_Workshop=true;
            showSnakBar(R.color.colorPrimary," Asistant: "+ String.valueOf(Is_Asistant_Workshop));
        } else {
            Is_Asistant_Workshop=false;
            showSnakBar(R.color.colorPrimary," Asistant: "+ String.valueOf(Is_Asistant_Workshop));
        }
    }



    //Ocultar teclado
    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        } catch (NullPointerException npe) {
            Log.e(getLocalClassName(), Log.getStackTraceString(npe));
        }
    }


    private void animationsLoad() {
        if (Build.VERSION.SDK_INT >= 21) {
            ///setupWindowAnimations();
            Window window = getWindow();
            setupWindowAnimations(window);
            setupWindowAnimations();
            animationButton();
        }
    }

    private void setupWindowAnimations(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(2000);
            android.transition.Fade t2 = new android.transition.Fade();
            window.setEnterTransition(t2);

            Slide slide = null;
            slide = new Slide();
            slide.setDuration(2000);
            window.setReturnTransition(slide);
        }

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        getWindow().setReenterTransition(new Explode());
        getWindow().setExitTransition(new Explode().setDuration(500));
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void launch(MainActivity context, int position) {
        Intent intent = new Intent(context, LoginActivity.class);
        // intent.putExtra(EXTRA_POSITION, position);
        // Los elementos 4, 5 y 6 usan elementos compartidos,
        ActivityOptions options0 = null;
        options0 = ActivityOptions.makeSceneTransitionAnimation(context);
        context.startActivity(intent, options0.toBundle());
        closed(context);
       // context.finishAfterTransition();
    }

    //Cerrar Activity Anterior
    private static void closed(final MainActivity context) {
        int myTimer= 3000;
        /// Toast.makeText(this, "Cargando...", Toast.LENGTH_LONG).show();
        new android.os.Handler().postDelayed(new Runnable(){
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run(){
                context.finish();
            }
        }, myTimer);
    }



    ///Instance
    /*----------------------------------------------------------------------------------------------------------------*/
    private static LoginActivity _instance;

    public LoginActivity() {
        _instance = this;
    }

    public static LoginActivity getInstance() {
        if (_instance == null) {
            _instance = new LoginActivity();
        }
        return _instance;
    }

    ///Animacion de Boton
    /*----------------------------------------------------------------------------------------------------------------*/
    private void animationButton() {
        sign_in_button.setScaleX(0);
        sign_in_button.setScaleY(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Interpolator interpolador = AnimationUtils.loadInterpolator(getBaseContext(),
                    android.R.interpolator.fast_out_slow_in);
            sign_in_button.animate()
                    .scaleX(1)
                    .scaleY(1)
                    .setInterpolator(interpolador)
                    .setDuration(600)
                    .setStartDelay(1000)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {


                        }
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            sign_in_button.animate()
                                    .scaleY(1)
                                    .scaleX(1)
                                    .setInterpolator(interpolador)
                                    .setDuration(600)
                                    .start();
                        }
                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }
                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
    }


    //Method Login
    /*----------------------------------------------------------------------------------------------------*/
    private void attemptLogin() {
        // Reset errors.
        etNumeroCedula.setError(null);
        //mPasswordView.setError(null);
        // Store values at the time of the login attempt.
        String email = etNumeroCedula.getText().toString();
        ///String password = mPasswordView.getText().toString();
        boolean cancel = false;
        View focusView = null;
        // Check for a valid password, if the user entered one.
      /*  if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }*/
        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            etNumeroCedula.setError(getString(R.string.error_field_required));
            focusView = etNumeroCedula;
            cancel = true;
        }
        /*else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }*/
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            progressBarIndeterminate.setVisibility(View.VISIBLE);
            if(isOnlineNet()){
                //showSnakBar(R.color.colorPrimary," Conectado a Internet");
                //hideProgressDialog();
                loginRequest(etNumeroCedula.getText().toString());

               /// LoginUserTask();
            }else{

                loginSqlite(etNumeroCedula.getText().toString());
            }
            /// mProgressView.setIndeterminate(true);
        }
    }

    //Mostrar Mensage Snackbar
    /*--------------------------------------------------------------------------------------------------------*/
    private void showSnakBar(int colorPrimary,String message) {
        int color = Color.WHITE;
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.login_form), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(this,colorPrimary));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_home_bar, 0, 0, 0);
       // textView.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin));
        textView.setTextColor(color);
        snackbar.show();
    }

    /*
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }*/

    ////Verificar conexiones a internet
    /*------------------------------------------------------------------------------*/
    public Boolean isOnlineNet() {

        try {
            Process p = Runtime.getRuntime().exec("ping -c 1 www.google.com");

            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }






    //Verificar si el usuario Existe
    /*--------------------------------------------------------------------------------------------------------*/
    private void loginRequest(final String NumberDocument) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Const.URL_RouteBaseAddress+VERIFICATE_USER_EXIST+NumberDocument, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d(TAG, response.toString());
                try {
                    // Parsing json object response
                    // response will be a json object
                    String ClienteExisteResult = response.getString("ClienteExisteResult");
                   //// Toast.makeText(getApplicationContext(), "Si existe es: "+ClienteExisteResult, Toast.LENGTH_SHORT).show();
                    if(ClienteExisteResult.equals("1")){
                        //Cargar informacion del usuario
                        LoadDatesUser(NumberDocument);
                    }else{
                        showSnakBar(R.color.OrangeColor,"Usuario no existe !");
                    }

                    /*String email = response.getString("email");
                    JSONObject phone = response.getJSONObject("phone");
                    String home = phone.getString("home");
                    String mobile = phone.getString("mobile");

                    jsonResponse = "";
                    jsonResponse += "Name: " + name + "\n\n";
                    jsonResponse += "Email: " + email + "\n\n";
                    jsonResponse += "Home: " + home + "\n\n";
                    jsonResponse += "Mobile: " + mobile + "\n\n";

                    txtResponse.setText(jsonResponse);*/

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();

                    progressBarIndeterminate.setVisibility(View.GONE);
                }
                progressBarIndeterminate.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                if (error.networkResponse.statusCode == 404) {
                    Toast.makeText(LoginActivity.this,"No se encontraron resultados",Toast.LENGTH_SHORT).show();
                }
                // hide the progress dialog
               // hidepDialog();
                progressBarIndeterminate.setVisibility(View.GONE);
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }



/*
    public void LoadInfoUser(final String NumberDocument) {

        try{
          //  final Usuario resultsLogin = new Usuario();
            StringRequest jsonObjRequest = new StringRequest(Request.Method.GET,
                    Const.URL_RouteBaseAddress+VERIFICATE_USER_DETAILS+NumberDocument,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            JSONObject obj = null;
                            Toast.makeText(getApplicationContext(), "Con datos: ", Toast.LENGTH_SHORT).show();

                            try {
                                obj = new JSONObject(response);

                                hideProgressDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideProgressDialog();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("volley", "Error: " + error.getMessage());
                    error.printStackTrace();
                    hideProgressDialog();
                    if (error.networkResponse.statusCode == 404) {
                        Toast.makeText(LoginActivity.this,"Usuario o contraseña incorrectos",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjRequest);
        } catch (Exception e) {
            Toast.makeText(LoginActivity.this,"Error: "+e,Toast.LENGTH_SHORT).show();
            hideProgressDialog();
        }
    }
*/
    ////Obtener informacion del usuario
    /*--------------------------------------------------------------------------------------------------------*/
    private void LoadDatesUser(final String NumberDocument) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Const.URL_RouteBaseAddress+VERIFICATE_USER_DETAILS+NumberDocument, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d(TAG, response.toString());
                try {
                    final User resultDetailUser = new User();
                    // Parsing json object response
                    // response will be a json object
                    JSONArray clienteDatosResultJsonResponse = response.getJSONArray("ClienteDatosResult");
                    String jsonResponse = "";
                    if(clienteDatosResultJsonResponse.length()>0){
                        int count = 0;
                        for (int i = 0; i < clienteDatosResultJsonResponse.length(); i++) {
                            JSONObject detail = (JSONObject) clienteDatosResultJsonResponse
                                    .get(i);
                            count= count+1;
                            if(count==1){

                                resultDetailUser.setCiudad(detail.getString("CIUDAD"));
                                resultDetailUser.setCodigo_Cliente(detail.getString("CODIGO_CLIENTE"));
                                resultDetailUser.setDireccion(detail.getString("DIRECCION"));
                                resultDetailUser.setDireccion_Electronica(detail.getString("DIRECCION_ELECTRONICA"));
                                resultDetailUser.setFecha_Nacimiento(detail.getString("FECHA_NACIMIENTO"));
                                resultDetailUser.setNombre_Cliente(detail.getString("NOMBRE_CLIENTE"));
                                resultDetailUser.setNumero_Cedula(NumberDocument);
                                resultDetailUser.setSexo(detail.getString("SEXO"));
                                resultDetailUser.setTelf_Fijo(detail.getString("TELF_FIJO"));
                                resultDetailUser.setTelf_Movil(detail.getString("TELF_MOVIL"));

                            }
                        }

                    }else{
                        resultDetailUser.setNumero_Cedula(NumberDocument);
                    }

                    userControllerInstance.abrirBaseDeDatos();
                    userControllerInstance.deleteUsers();
                    userControllerInstance.insertUser(resultDetailUser.getNombre_Cliente(),
                            resultDetailUser.getCodigo_Cliente(),resultDetailUser.getNumero_Cedula(),
                            Integer.parseInt(resultDetailUser.getNumero_Cedula()), resultDetailUser.getDireccion(),
                            resultDetailUser.getCiudad(),resultDetailUser.getTelf_Fijo(),
                            resultDetailUser.getTelf_Movil(), resultDetailUser.getDireccion_Electronica(),
                            resultDetailUser.getFecha_Nacimiento(),resultDetailUser.getSexo(),Is_Asistant_Workshop,
                            0,Is_Remembered);
                    userControllerInstance.cerrar();
                    //Add more properties
                    resultDetailUser.setIs_Admin(Is_Asistant_Workshop);
                    resultDetailUser.setIsRemembered(Is_Remembered);
                    resultDetailUser.setWorkshop_Id(0);

                    sendMenuLogin(resultDetailUser);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    progressBarIndeterminate.setVisibility(View.GONE);
                }
                ///progressBarIndeterminate.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                if (error.networkResponse.statusCode == 404) {
                    Toast.makeText(LoginActivity.this,"No se encontraron resultados",Toast.LENGTH_SHORT).show();
                }
                // hide the progress dialog
                // hidepDialog();
                progressBarIndeterminate.setVisibility(View.GONE);
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    /*Login SQLITE*/
    /*-------------------------------------------------------------------------------------------------------------------*/
    private void loginSqlite(String Numero_Cedula){
        userControllerInstance.abrirBaseDeDatos();
        User userFirts= userControllerInstance.getFirstUser();
        if(userFirts.getUser_Id()>0){
            User userLogin= userControllerInstance.loginUser(Numero_Cedula);
            if(userLogin.getUser_Id()>0){
                sendMenuLogin(userLogin);
                userControllerInstance.cerrar();
            }
            else{
                showSnakBar(R.color.OrangeColor,"Identificacion ingresada incorrecta !");
                progressBarIndeterminate.setVisibility(View.GONE);
                userControllerInstance.cerrar();
            }
        }else{


            showSnakBar(R.color.OrangeColor,"Verifique su conexion a internet !");
            progressBarIndeterminate.setVisibility(View.GONE);
            userControllerInstance.cerrar();
        }
    }


    /*Login Verifique*/
    /*-------------------------------------------------------------------------------------------------------------------*/
    private void verificateLoginIsRemembered() {
        userControllerInstance.abrirBaseDeDatos();
        User user= userControllerInstance.getFirstUser();
        if(user.getUser_Id()>0 && user.getIsRemembered()==true){
            sendMenuLogin(user);
            userControllerInstance.cerrar();
        }
    }

    ///Enviar al menu
    /*---------------------------------------------------------------------------------------------------*/
    private void sendMenuLogin(User resultDetailUser) {
        //Si es administrador
        if(resultDetailUser.getIs_Admin()==true){
            if (Build.VERSION.SDK_INT >= 21) {
                int position = 1;
                Menu_Workshop_Assistant_Activity.launch(
                        LoginActivity.this, position);
                progressBarIndeterminate.setVisibility(View.GONE);
            } else {
                Intent i = new Intent(LoginActivity.this, Menu_Workshop_Assistant_Activity.class);
                startActivity(i);
                progressBarIndeterminate.setVisibility(View.GONE);
            }
        } ///Si no es administrador
        else{
            if (Build.VERSION.SDK_INT >= 21) {
                int position = 1;
                MenuActivity.launch(
                        LoginActivity.this, position);
                progressBarIndeterminate.setVisibility(View.GONE);
            } else {
                Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(i);
                progressBarIndeterminate.setVisibility(View.GONE);
            }
        }
    }

    /*Navegar hacia atras*/
    @Override
    public void onBackPressed() {
        finish();
    }

}

