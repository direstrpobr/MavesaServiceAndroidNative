package com.themakers.mavesaservice.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;

import com.themakers.mavesaservice.R;

public class AppoinmentActivity extends AppCompatActivity {

    GridView mGridView;
    int[] mToday= new int[3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoinment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*
        Calendar mCalendar = Calendar.getInstance();

        mToday[0] = mCalendar.get(Calendar.DAY_OF_MONTH);
        mToday[1] = mCalendar.get(Calendar.MONTH); // zero based
        mToday[2] = mCalendar.get(Calendar.YEAR);


// get display metrics
        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);


// set adapter
        mGridView = (GridView)findViewById(R.id.gridview);
        mGridView.setAdapter(new MonthAdapter(this, mToday[1], mToday[2], metrics) {
            @Override
            protected void onDate(int[] date, int position, View item) {

            }
        });

*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
