package com.themakers.mavesaservice.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.themakers.mavesaservice.Activities.LoginActivity;
import com.themakers.mavesaservice.Conf.DataSource;
import com.themakers.mavesaservice.Models.User;

/**
 * Created by EnuarMunoz on 05/08/2017.
 */


public class UserController {
    private DataSource dbhelper;
    private SQLiteDatabase database;
    private static Context ourcontext;
    private static UserController _instance;

    /*
    public UserController(Context c) {
        ourcontext = c;
        _instance = this;
    }*/

    ///Instance
    /*----------------------------------------------------------------------------------------------------------------*/
    public UserController() {
        _instance = this;
    }

    public static UserController getInstance(Context c) {
        if (_instance == null) {
             ourcontext = c;
            _instance = new UserController();
        }
        return _instance;
    }


    public UserController abrirBaseDeDatos() throws SQLException {
        dbhelper = new DataSource(ourcontext);
        database = dbhelper.getWritableDatabase();
        return this;
    }


    public void cerrar() {
        dbhelper.close();
    }
    ////Insertar Datos Usuarios
    public void insertUser(String Nombre_Cliente,String Codigo_Cliente,String Numero_Cedula,int Identificacion,String Direccion,String Ciudad,String Telf_Fijo,String Telf_Movil, String Direccion_Electronica, String Fecha_Nacimiento, String Sexo,boolean Is_Admin, int Workshop_Id,boolean IsRemembered) {
        ContentValues cv = new ContentValues();
        cv.put(DataSource.Nombre_Cliente, Nombre_Cliente);
        cv.put(DataSource.Codigo_Cliente, Codigo_Cliente);
        cv.put(DataSource.Numero_Cedula, Numero_Cedula);
        cv.put(DataSource.Identificacion, Identificacion);
        cv.put(DataSource.Direccion, Direccion);

        cv.put(DataSource.Ciudad, Ciudad);
        cv.put(DataSource.Telf_Fijo, Telf_Fijo);
        cv.put(DataSource.Telf_Movil, Telf_Movil);
        cv.put(DataSource.Direccion_Electronica, Direccion_Electronica);
        cv.put(DataSource.Fecha_Nacimiento, Fecha_Nacimiento);
        cv.put(DataSource.Sexo, Sexo);
        cv.put(DataSource.Is_Admin, Is_Admin);
        cv.put(DataSource.Workshop_Id, Workshop_Id);
        cv.put(DataSource.IsRemembered, IsRemembered);

        database.insert(DataSource.TABLE_USERS, null, cv);

    }

    ///Eliminar Users bd
    public void deleteUsers() {
        dbhelper = new DataSource(ourcontext);
        database = dbhelper.getWritableDatabase();
        database.delete(DataSource.TABLE_USERS, null, null);
        //String select = "delete from Sincronizacion";
        //database.execSQL(select);
    }

    /// Actualizar
    public  void updateLoginSesion(int idUser, boolean Estado_Sesion){
        ContentValues cv = new ContentValues();
        cv.put(DataSource.IsRemembered, Estado_Sesion);
        database.update(DataSource.TABLE_USERS, cv,DataSource.User_Id+"="+ idUser, null);
    }

    ////Login Usuario
    public User loginUser(String Numero_Cedula) {
        dbhelper = new DataSource(ourcontext);
        database = dbhelper.getWritableDatabase();
        String select = "select * from "+DataSource.TABLE_USERS+" where "+DataSource.Numero_Cedula+" = '"+Numero_Cedula+"'";
        Cursor cursor = database.rawQuery(select, null);
        ///Cursor cursor = database.query(table, columns, null, null, null, null, null);
        User user = new User();
        /*
        while(cursor.moveToNext()) {
            Usuario usuario = cursorToNote(cursor);
            list.add(usuario);
        }*/
        if (cursor.moveToFirst()){
            do {
                user.setUser_Id(cursor.getInt(0));
                user.setNombre_Cliente(cursor.getString(1));
                user.setCodigo_Cliente(cursor.getString(2));
                user.setNumero_Cedula(cursor.getString(3));
                user.setIdentificacion(cursor.getInt(4));
                user.setDireccion(cursor.getString(5));
                user.setCiudad(cursor.getString(6));
                user.setTelf_Fijo(cursor.getString(7));
                user.setTelf_Movil(cursor.getString(8));
                user.setDireccion_Electronica(cursor.getString(9));
                user.setFecha_Nacimiento(cursor.getString(10));
                user.setSexo(cursor.getString(11));
                user.setIs_Admin(cursor.getInt(12)> 0);
                user.setWorkshop_Id(cursor.getInt(13));
                user.setIsRemembered(cursor.getInt(14)> 0);
            }while (cursor.moveToNext());
        }
        return user;
    }

    ////cargar total leidos
    public User getFirstUser() {
        dbhelper = new DataSource(ourcontext);
        database = dbhelper.getWritableDatabase();
        User results = new User();
        String select = "select * from "+DataSource.TABLE_USERS;
        Cursor cursor = database.rawQuery(select, null);
        //If Cursor is valid
        if (cursor.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                // String conf = cursor.getString(0);
                results.setUser_Id(cursor.getInt(0));
                results.setNombre_Cliente(cursor.getString(1));
                results.setCodigo_Cliente(cursor.getString(2));
                results.setNumero_Cedula(cursor.getString(3));
                results.setIdentificacion(cursor.getInt(4));
                results.setDireccion(cursor.getString(5));
                results.setCiudad(cursor.getString(6));
                results.setTelf_Fijo(cursor.getString(7));
                results.setTelf_Movil(cursor.getString(8));
                results.setDireccion_Electronica(cursor.getString(9));
                results.setFecha_Nacimiento(cursor.getString(10));
                results.setSexo(cursor.getString(11));
                results.setIs_Admin(cursor.getInt(12)> 0);
                results.setWorkshop_Id(cursor.getInt(13));
                results.setIsRemembered(cursor.getInt(14)> 0);
            } while(cursor.moveToNext());
        }
        return results;
    }

}
