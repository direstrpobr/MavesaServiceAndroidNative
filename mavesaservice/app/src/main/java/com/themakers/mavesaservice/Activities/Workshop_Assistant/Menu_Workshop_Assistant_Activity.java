package com.themakers.mavesaservice.Activities.Workshop_Assistant;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.themakers.mavesaservice.Activities.LoginActivity;
import com.themakers.mavesaservice.Activities.MenuActivity;
import com.themakers.mavesaservice.Controllers.UserController;
import com.themakers.mavesaservice.Models.User;
import com.themakers.mavesaservice.R;

public class Menu_Workshop_Assistant_Activity extends AppCompatActivity {

    //SQLITE
    UserController userControllerInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__workshop__assistant_);

        setToolbar();

        AnimationsLoad();

        Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();

        loadInstances();

        welcome();

    }

    private void welcome() {
        showSnakBar(R.color.LightRedColor,"Bienvenido Asistente de taller");
    }

    ///Toolbar
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    ///Instances Class
    /*------------------------------------------------------------------------------------*/
    private void loadInstances() {
        this.userControllerInstance= UserController.getInstance(this);
    }
    //END INSTANCE
   /*------------------------------------------------------------------------------------*/

    /*ANIMATIONS*/
    /*-----------------------------------------------------------------------------------*/
    private void AnimationsLoad() {
        if (Build.VERSION.SDK_INT >= 21){
            setupWindowAnimations();
            Window window = getWindow();
            Transition t3 = TransitionInflater.from(this)
                    .inflateTransition(R.transition.detail_enter_transition);
            window.setEnterTransition(t3);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        getWindow().setReenterTransition(new Explode());
        getWindow().setExitTransition(new Explode().setDuration(500));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void launch(LoginActivity context, int position) {
        Intent intent = new Intent(context, Menu_Workshop_Assistant_Activity.class);
        // intent.putExtra(EXTRA_POSITION, position);
        // Los elementos 4, 5 y 6 usan elementos compartidos,
        ActivityOptions options0 = ActivityOptions.makeSceneTransitionAnimation(context);
        context.startActivity(intent,options0.toBundle());
        closedActivity(context);
        //context.finishAfterTransition();
    }

    //Cerrar Activity Anterior
    private static void closedActivity(final LoginActivity context) {
        int myTimer= 2000;
        /// Toast.makeText(this, "Cargando...", Toast.LENGTH_LONG).show();
        new android.os.Handler().postDelayed(new Runnable(){
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run(){
                context.finish();
            }
        }, myTimer);
    }

     /*END  ANIMATIONS*/
    /*-----------------------------------------------------------------------------------*/



    /*METHOD VOLVER*/
    /*-----------------------------------------------------------------------------------------------*/
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmacion");
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i("Dialogos", "Confirmacion Cancelada.");
            }
        });
        builder.setMessage("¿Cerrar Sesión?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                userControllerInstance.abrirBaseDeDatos();
                User user= userControllerInstance.getFirstUser();
                userControllerInstance.updateLoginSesion(user.getUser_Id(),false);
                startActivity(new Intent(getBaseContext(), LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                userControllerInstance.cerrar();
                finish();
            }
        });
        builder.setIcon(R.drawable.ic_logo_mavesa_service);
        builder.show();
    }



    //Mostrar Mensage Snackbar
    /*--------------------------------------------------------------------------------------------------------*/
    private void showSnakBar(int colorPrimary,String message) {
        int color = Color.WHITE;
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.menuWorkshopAsistant), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(this,colorPrimary));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_home_bar, 0, 0, 0);
        // textView.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin));
        textView.setTextColor(color);
        snackbar.show();
    }

}
